package com.zut.pointsofinterest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by BEAR on 22.05.2017.
 */

public class MySQLite extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;


    public void addPoiElement (PoiElement el) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("author", el.getAuthor());
        values.put("description", el.getDescription());
        values.put("image_file_name", el.getImageFileName());
        values.put("pos_long", el.getPositionLongitude());
        values.put("pos_lang", el.getPositionLatitude());
        values.put("title", el.getTitle());
        values.put("address", el.getAddress());

        db.insert("poi_elements", null, values);
    }

    public void deletePoiElement(int ID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("poi_elements", "_id = ?", new String[] { String.valueOf(ID) });
        db.close();
    }

    public int updatePoiElement(PoiElement el) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("author", el.getAuthor());
        values.put("description", el.getDescription());
        values.put("image_file_name", el.getImageFileName());
        values.put("pos_long", el.getPositionLongitude());
        values.put("pos_lang", el.getPositionLatitude());
        values.put("title", el.getTitle());
        values.put("address", el.getAddress());

        int i = db.update("poi_elements", values, "_id = ?", new String[]{String.valueOf(el.getId())});

        db.close();

        return i;
    }

    public PoiElement getPoiElement( int ID) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("poi_elements",
                new String[] { "_id", "author", "description", "image_file_name", "pos_long", "pos_lang", "title", "address"}, // nazwy kolumn do pobrania
                "_id = ?", // po czym szukamy
                new String[] {String.valueOf(ID)},
                null, // group by
                null, //having
                null, //order by
                null); // limit

        if (cursor != null) {
            cursor.moveToFirst();
        }

        PoiElement el = new PoiElement(cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getFloat(4), cursor.getFloat(5), cursor.getString(6), cursor.getString(7));

        el.setId(Integer.parseInt(cursor.getString(0)));

        return el;
    }

    public ArrayList<PoiElement> getPoiElements() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM poi_elements", null);

        ArrayList<PoiElement> mArrayList = new ArrayList<PoiElement>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            //mArrayList.add(c.getString(c.getColumnIndex(dbAdapter.KEY_NAME))); //add the item
            PoiElement el = new PoiElement(cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getFloat(4), cursor.getFloat(5), cursor.getString(6), cursor.getString(7));

            el.setId(Integer.parseInt(cursor.getString(0)));

            mArrayList.add(el);
            cursor.moveToNext();
        }

        return mArrayList;
    }


    //region Constructor and DB creation
    public MySQLite(Context context) {
        super(context, "poiDB", null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase database) {
        String DATABASE_CREATE = "CREATE TABLE poi_elements (" +
                "_id integer primary key autoincrement," +
                "author TEXT NOT NULL," +
                "description TEXT NOT NULL," +
                "image_file_name TEXT NOT NULL," +
                "pos_long REAL NOT NULL," +
                "pos_lang REAL NOT NULL," +
                "title TEXT NOT NULL," +
                "address TEXT NOT NULL);";
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS poi_elements");
        onCreate(db);
    }
    //endregion

}
