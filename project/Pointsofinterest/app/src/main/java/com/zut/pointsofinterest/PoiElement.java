package com.zut.pointsofinterest;

import java.io.Serializable;

/**
 * Created by BEAR on 22.05.2017.
 */

public class PoiElement implements Serializable {

    //region Vars
    private int id;
    private String author;
    private String description;
    private String imageFileName;
    private float positionLongitude;
    private float positionLatitude;
    private String title;
    private String address;
    //endregion




    //region Contructors
    public PoiElement () {}

    public PoiElement(String author, String description, String imageFileName, float positionLongitude, float positionLatitude, String title, String address) {
        this.author = author;
        this.description = description;
        this.imageFileName = imageFileName;
        this.positionLongitude = positionLongitude;
        this.positionLatitude = positionLatitude;
        this.title = title;
        this.address = address;
    }

    //endregion




    //region Getters
    public int getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public String getImageFileName() {
        return imageFileName;
    }

    public float getPositionLongitude() {
        return positionLongitude;
    }

    public float getPositionLatitude() {
        return positionLatitude;
    }

    public String getTitle() {
        return title;
    }

    public String getAddress() {
        return address;
    }

    //endregion


    //region Setters
    public void setId(int id) {
        this.id = id;
    }
    //endregion
}
