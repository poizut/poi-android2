package com.zut.pointsofinterest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity {

    private GridView gridView;
    private ImageAdapter gridAdapter;
    private MySQLite db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        db = new MySQLite(getApplicationContext());
        gridView = (GridView) findViewById(R.id.main_grid);
        gridAdapter = new ImageAdapter(this, R.layout.grid_image, db.getPoiElements());
        gridView.setAdapter(gridAdapter);
    }
}
